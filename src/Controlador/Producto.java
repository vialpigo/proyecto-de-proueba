/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.ConexionBD;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Victor Alfonso
 */
public class Producto {
    private String id;
    private String nombre;
    private double temperatura;
    private double valorBase;
    
    
    public Producto(){}

    public Producto(String nombre, String id, double temperatura, double valorBase) {
        this.id = id;
        this.nombre = nombre;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }
    
    public boolean guardarProducto(){
        ConexionBD conexion=new ConexionBD();
        String sql="insert into producto (id, nombre,temperatura,valorbase)"+
                "values ('"+id+"','"+nombre+"',"+temperatura+","+valorBase+")";
        
        if(conexion.insertarBD(sql)){
            conexion.cerrarConexion();
            return true;
        }else{
            conexion.cerrarConexion();
            return false;
        }        
    }
    
    public List<Producto> listarProducto(){
        List<Producto> listaProducto=new ArrayList<>();
        ConexionBD conexion=new ConexionBD();
        String sql="select * from producto";
        try{
            ResultSet rs=conexion.consultarBD(sql);
            while(rs.next()){
                listaProducto.add(
                new Producto(        
                        rs.getString("nombre")
                        ,rs.getString("id")
                        ,rs.getDouble("temperatura")
                        ,rs.getDouble("valorBase")
                )
                );
            }
        }catch(Exception e){
            System.out.print("Erro al consultar los productos");
        }
        conexion.cerrarConexion();
        
        return listaProducto;
        
    }
    
    public boolean actualizarProducto(){
        ConexionBD conexion=new ConexionBD();
        String sql="update producto set "
                +"nombre='"+nombre+"',"
                +"temperatura="+temperatura+","
                +"valorBase="+valorBase+" "
                +"where id='"+id+"'";
        
        if(conexion.actualizarBD(sql)){
            conexion.cerrarConexion();
            return true;
        }else{
            conexion.cerrarConexion();
            return false;
        }
        

    }
    
     public boolean eliminarProducto(){
        ConexionBD conexion=new ConexionBD();
        String sql="delete from producto "
                    +"where id='"+id+"'";
        
        if(conexion.actualizarBD(sql)){
            conexion.cerrarConexion();
            return true;
        }else{
            conexion.cerrarConexion();
            return false;
        }
        

    }
     public List<Producto> listarProducto(String where){
        List<Producto> listaProducto=new ArrayList<>();
        ConexionBD conexion=new ConexionBD();
        String sql="select * from producto where "+where;
         System.out.println(sql);
        try{
            ResultSet rs=conexion.consultarBD(sql);
            while(rs.next()){
                listaProducto.add(
                new Producto(        
                        rs.getString("nombre")
                        ,rs.getString("id")
                        ,rs.getDouble("temperatura")
                        ,rs.getDouble("valorBase")
                )
                );
            }
        }catch(Exception e){
            System.out.print("Erro al consultar los productos");
        }
        conexion.cerrarConexion();
        
        return listaProducto;
        
    } 

    @Override
    public String toString() {
        return "Producto{" + "id=" + id + ", nombre=" + nombre + ", temperatura=" + temperatura + ", valorBase=" + valorBase + '}';
    }
     
}
